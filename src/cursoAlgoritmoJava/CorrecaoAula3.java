package cursoAlgoritmoJava;

public class CorrecaoAula3 {

	public static void main(String[] args) {
		/* Vamos fazer um exercício e usar uma estrutura de repetição para imprimir APENAS os números pares.
		 * Pra fazer esse exercício você vai precisar usar:
		 * - um IF 
		 * - do operador matemático que recupera o resto da divisão: %
		 * exemplo: int resultado = 9 % 3; vai te devolver o valor 0
		 * */
		
		//Contador
		int contador = 0;
		//Condição do while
		while(contador <= 10 ) {
			//Estrutura condicional verificando se é um valor par
			if(contador % 2 == 0) {
				//System.out.println(contador);
			}
			contador++;
		}
		
		contador = 0;
		for(;contador <= 20;) {
			if(contador % 2 == 0) {
				//System.out.println(contador);
			}
			contador++;
		}
		
		contador = 0;
		do {
			if(contador % 2 != 0) {
				System.out.println(contador);
			}
			contador++;
		} while (contador <= 20);
	}
	
}
