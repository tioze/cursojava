package cursoAlgoritmoJava;

import java.util.Scanner;

public class Aula3 {

	public static void main(String[] args) {
		
		double precocafe = 8.50;
		double preco_cafe_mercado_x = 8.50;
		
		if(precocafe > preco_cafe_mercado_x) {
			//System.out.println("Preço alto");
		}
		/*
		 * ELSE significa OU em inglês. Usamos para indicar quando uma operação IF não é verdadeira.
		 * 
		if(precocafe < preco_cafe_mercado_x) {
			System.out.println("Preço alto");
		} else if (precocafe > preco_cafe_mercado_x) {
			System.out.println("Preço barato");
		} else {
			System.out.println("Preços iguais.");
		}*/
		
		Scanner console = new Scanner(System.in);
		
		double preco_cafe_mercado_y = 0;
        System.out.println("Digite o preço do café no mercado x por favor : ");
        preco_cafe_mercado_x = console.nextDouble();
        System.out.println("Digite o preço do café no mercado y por favor: ");
        preco_cafe_mercado_y = console.nextDouble();
        System.out.println("Preço no mercado X : " + preco_cafe_mercado_x);
        System.out.println("Preço no mercado Y : " + preco_cafe_mercado_y);
        
        console.close();
        /*
         * Dever de casa:
         * alterar esse código para incluir o mercado Z, comparar os preços e dizer qual é o mais barato.
         * 
		*/
	}
	
}
