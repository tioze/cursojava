package cursoAlgoritmoJava;

import java.util.Arrays;
import java.util.List;

public class Aula4 {

	public static void main(String[] args) {
		/* Existem muitos tipos de listas e filas em Java e muitas maneiras de inicializar uma varíavel que seja uma lista.
		 * A mais usada no dia a dia de um profissional é List
		 * 
		 * No Java, nós podemos dizer que tipo de conteúdo vai ser estocado na lista. Então vamos iniciar uma lista que tenha strings, ou seja: texto.
		 * */
		List<String> listaNomes = Arrays.asList("João", "Maria", "Zé");
		/* Esta lista tem 3 itens e cada item tem uma posição na lista.
		 * */
		
		/* No Java existem muitas formas de fazer ESTRUTURAS DE REPETIÇÃO
		 * o FOR e o equivalente ao PARA CADA.
		 * 
		 * Abaixo uma forma comum para criarmos um BLOCO PARA
		 * */
		for(String temporaria : listaNomes) {
			//Cada item a lista vai ser colocada numa variável com o nome temporaria. Variáveis criadas dentro de um bloco NÃO EXISTEM FORA DELE.
			//System.out.println(temporaria);
		}
		/* Execute o bloco acima e confira o resultado.
		 * Esse tipo de FOR é o melhor quando já temos uma lista pronta!
		 * */
		
		/*
		 * Outra maneira de fazer o mesmo bloco é a seguinte:
		 * */
		for(int i = 0 ; i < listaNomes.size() ; i++) {
			//String temporaria = listaNomes.get(i);
			//System.out.println(temporaria);
		}
		/* Execute o código acima e você vai ter o mesmo resultado do bloco anterior.
		 * Mas aqui incluímos um tipo de variável muito usada também: CONTADOR. Vamos analisar a linha 32 calmamente.
		 * 1 - criamos uma estrutura de repetição do tipo FOR que recebe 3 instruções
		 * 		1 - criamos uma variável int,  ou seja, um número inteiro com valor inicial 0 -----> int i = 0
		 * 		2 - instruímos que este bloco deve executar até o tamanho da lista ------> i < listaNomes.size()
		 * 		3 - no fim da execução, queremos que incremente a variável i com mais 1 número!
		 * 
		 * Muitas linguagens usam essa estrutura. E muitas vezes precisamos de um contador para resolver algum problema.
		 * */
		
		
		
		/* Vamos ver mais dois tipos de estrutura de repetição:
		 * WHILE - quer dizer ENQUANTO.
		 * Criamos um bloco de instruções que vão ser executados ENQUANTO uma determinada condição for verdadeira.
		 * Vamos ao exemplo!
		 * */
		int contador = 0;
		while(contador <= 10 ) {
			//System.out.println(contador);
			contador++;//Isso é a mesma coisa que escrever: contador = contador + 1;
		}
		
		
		/* Agora vamos ver ainda mais uma estrutura de repetição:
		 * DO/WHILE - DO é um verbo do inglês que significa fazer. Nas linguagens de programação está no modo imperativo ou seja: FAÇA.
		 * Abaixo nós criamos um bloco que diz exatamente isso: FAÇA algo ENQUANTO essa condição for verdadeira.
		 * */
		contador = 0;
		do {
			//System.out.println(contador);
			contador++;//Isso é a mesma coisa que escrever: contador = contador + 1;
		} while(contador <= 20);
		
		/* Vamos fazer um exercício e usar uma estrutura de repetição para imprimir APENAS os números pares.
		 * Pra fazer esse exercício você vai precisar usar:
		 * - um IF 
		 * - do operador matemático que recupera o resto da divisão: %
		 * exemplo: int resultado = 9 % 3; vai te devolver o valor 0
		 * */
	}

}
