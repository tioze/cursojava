package cursoAlgoritmoJava;

import java.util.Scanner;

public class CorrecaoAula2 {

	public static void main(String[] args) {
		/*
         * Dever de casa:
         * alterar esse código para incluir o mercado Z, comparar os preços e dizer qual é o mais barato.
		*/
		double preco_cafe_mercado_x = 0;
		double preco_cafe_mercado_y = 0;
		double preco_cafe_mercado_z = 0;
		double menorPreco = 0;
		
		Scanner console = new Scanner(System.in);
		
        System.out.println("Digite o preço do café no mercado x por favor : ");
        preco_cafe_mercado_x = console.nextDouble();
        menorPreco = preco_cafe_mercado_x;
        
        System.out.println("Digite o preço do café no mercado y por favor: ");
        preco_cafe_mercado_y = console.nextDouble();
        if(preco_cafe_mercado_y < menorPreco) {
        	menorPreco = preco_cafe_mercado_y;
        }
        
        System.out.println("Digite o preço do café no mercado Z por favor: ");
        preco_cafe_mercado_z = console.nextDouble();
        if(preco_cafe_mercado_z < menorPreco) {
        	menorPreco = preco_cafe_mercado_z;
        }

        System.out.println("Menor preço é : " + menorPreco);
        
        console.close();
        
	}
}
