package cursoAlgoritmoJava;

public class Aula2 {

	public static void main(String[] args) {		
		/*
		 * ESTRUTURAS CONDICIONAIS
		 * aprendemos a estrutura SE. Na linguagem Java e na maioria das linguagens criamos um bloco condicional usando IF que significa SE em inglês.
		 * A sintaxe Java exige que seja escrito como no exemplo abaixo:
		 * */
		int valor1 = 1;
		int valor2 = 1;
		if(valor1 == valor2) {//Este é um exemplo apenas para explicar a SINTAXE de uma estrutura condicional SE/IF
			//System.out.println("Variáveis são iguais.");
		}
		
		/* 
		 * Os nomes das variáveis em Java devem seguir algumas regras e em geral, serve para todas as linguagens.
		 * Nomes de variáveis não podem ter acentos, espaços em branco e símbolos. Também não podem usar palavras reservadas ou comandos: if, else e vários outros.
		 * 
		 * */
		double precocafe = 9.50;
		double preco_cafe_mercado_x = 8.50;
		
		if(precocafe > preco_cafe_mercado_x) {
			//System.out.println("Preço alto");
		}
		
	}
	
}
