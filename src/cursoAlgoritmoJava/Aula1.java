package cursoAlgoritmoJava;

public class Aula1 {
	
	public static void main(String[] args) {
		// System.out.println("Alô mundo");
		
		/* Tipo da variável (String) nome (alomundo) = valor inicial (Alô mundo) 
		 * String quer dizer texto.
		 * E esse texto que estamos lendo está num comentário, que são blocos que
		 * o Java ignora na hora de executar
		 * 
		 * Essa variável aí em baixo não faz nada, só guarda esse valor.
		 * */
		//String alomundo = "Alô mundo 2";
		
		/*
		 * Executando o código abaixo, vai imprimir no console o valor da variável.
		 * */
		//System.out.println(alomundo);		
	}

}
